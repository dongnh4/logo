//
//  LogoService.swift
//  LogoApp
//
//  Created by đông on 25/06/2022.
//

import Foundation
class apiCall {
	func getData(completion: @escaping ([News], [Event], [Category]) -> ()) {
		guard let url = URL(string: "https://demo8465056.mockable.io/all") else { return }
//		guard let url = URL(string: "https://jsonplaceholder.typicode.com/posts/1/comments") else { return }
//		guard let url = URL(string: "https://demo3119986.mockable.io/all") else { return }

		URLSession.shared.dataTask(with: url) { (data, _, _) in
			let data = try! JSONDecoder().decode(LogoModel.self, from: data!)
			print(data)

			DispatchQueue.main.async {
				completion(data.news, data.event, data.category)
			}
		}
		.resume()
	}
}
