//
//  LogoModel.swift
//  LogoApp
//
//  Created by đông on 18/06/2022.
//

import Foundation
import SwiftUI

enum stateSelected {
	case no
	case notSure
	case sure
}

struct News: Identifiable, Decodable {
	var id = UUID()
	let date: String
	let title: String
	let author: String
	let address: String
	let image: String

	enum CodingKeys: String, CodingKey {
		case date = "date"
		case title = "title"
		case author = "author"
		case address = "address"
		case image = "image"
	}
}

struct Event: Identifiable, Decodable {
    var id = UUID()
	let date: String
	let quantity: String
	var title: String
	let description: String
	let image: String
//	var state: stateSelected

	enum CodingKeys: String, CodingKey {
		case date = "date"
		case quantity = "quantity"
		case title = "title"
		case description = "description"
		case image = "image"
	}
}

struct Category: Identifiable, Decodable {
	var id = UUID()
	let name: String

	enum CodingKeys: String, CodingKey {
		case name = "name"
	}
}

struct LogoModel: Decodable {
	let news: [News]
	let event: [Event]
	let category: [Category]
        
	enum CodingKeys: String, CodingKey {
		case news = "News"
		case event = "Event"
		case category = "Category"
	}
}

class CategoryRepository{
	static let shared = CategoryRepository()

	func categoryData() -> [String]{
		return ["Arts & Culture","Career & Business","Cars & Motorcycles","Community & Environment","Dancing","Education & Learning","Fashion & Beauty","Fitness","Games","LGBT","Movements & Politics","Hobbies & Crafts","Language & Ethnic Identity",]
	}
}
