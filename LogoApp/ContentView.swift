//
//  ContentView.swift
//  LogoApp
//
//  Created by đông on 16/06/2022.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        MainLoginView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
