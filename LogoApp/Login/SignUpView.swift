//
//  SignUpView.swift
//  LogoApp
//
//  Created by DongNH on 17/06/2022.
//

import SwiftUI

private enum Constant {
	static let separateLineWidth = 2.0
	static let separateLineHeight = 35.0
	static let spacing = 20.0
}

struct SignUpView: View {
	// MARK: - Variables
	@State private var text = ""
	@State private var email = ""
	@State private var password = ""

	// MARK: - Body
    var body: some View {
		VStack(spacing: Constant.spacing) {
			ItemLogin("Họ và tên", "icon-profile", "Nhập họ và tên", textField: text)
			ItemLogin("Email", "icon-email", "abc@rikkeisoft.com", textField: email)
			ItemLogin("Mật khẩu", "icon-lock", "Độ dài 6-16 kí tự", textField: password)
		}
		.padding(.top, Constant.spacing)
	}
}

struct SignUpView_Previews: PreviewProvider {
    static var previews: some View {
        SignUpView()
    }
}
