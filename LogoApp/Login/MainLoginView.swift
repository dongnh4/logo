//
//  LoginView.swift
//  LogoApp
//
//  Created by đông on 16/06/2022.
//

import SwiftUI

private enum Constant {
    static let heightImage = 50.0
    static let heightButtonLogin = 50.0
    static let separateLineHeight = 2.0
}

struct MainLoginView: View {
    // MARK: - Variables
    @State private var didTapSignUp: Bool = false
    @State private var didTapLogin: Bool = false
    @State private var isHomeView: Bool = false
    
    // MARK: - Body
    var body: some View {
		NavigationView {
			VStack {
				logoImage
				optionButton
				optionView
				Spacer()
				signUpButton
					.padding(.bottom,50)
			}
			.navigationBarTitle("")
			.navigationBarHidden(true)
			.edgesIgnoringSafeArea(.all)
		}
    }
}

#if DEBUG
struct MainLoginView_Previews: PreviewProvider {
    static var previews: some View {
        MainLoginView()
    }
}
#endif

// MARK: - Extensions
private extension MainLoginView {
    var logoImage: some View {
        ZStack {
            Image("Mask Group (1)")
                .frame(height: Constant.heightImage)
                .padding(.top, 50)
            
            Image("logo")
                .padding(.top, 50)
        }
    }
    
    var optionButton: some View {
        HStack {
            VStack {
                Button (action: {
                    self.didTapSignUp = true
                    self.didTapLogin = false
                }) {
                    Text("Đăng kí")
                        .foregroundColor(didTapSignUp ? Color.black : Color.gray)
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                }
                
                if didTapSignUp {
                    Rectangle()
                        .frame(height: Constant.separateLineHeight)
                        .padding(.horizontal, 10)
                        .foregroundColor(Color.purple)
                }
            }
            
            VStack {
                Button {
                    didTapLogin = true
                    didTapSignUp = false
                } label: {
                    Text("Đăng nhập")
                        .foregroundColor(didTapLogin ? Color.black : Color.gray)
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                }
                if didTapLogin {
                    Rectangle()
                        .frame(height: Constant.separateLineHeight)
                        .padding(.horizontal, 10)
                        .foregroundColor(Color.purple)
                }
            }
        }
        .frame(maxWidth: .infinity)
        .frame(height: Constant.heightButtonLogin)
        .background(Color.clear)
        .padding(.top, 60)
    }
    
    var optionView: some View {
        Group {
            if didTapSignUp {
                SignUpView()
            } else {
                LoginView()
            }
        }
    }
    
	var signUpButton: some View {
		VStack {
			NavigationLink(destination: {
				VStack {
					if didTapSignUp {
						LoginView()
					} else {
						TabBarView()
					}
				}
			}(),
						   isActive: $isHomeView,
						   label: {
				Button {
					isHomeView.toggle()
				} label: {
					ZStack {
						Image("Rectangle 6")
						Text(didTapSignUp ? "Đăng kí" : "Đăng nhập")
							.foregroundColor(Color.white)
					}
				}
			})

			Button {

			} label: {
				Text("Bỏ qua")
					.foregroundColor(Color.black)
					.frame(maxWidth: .infinity)
			}
		}
	}
}

extension MainLoginView {
	func isLogin() {

	}
}
