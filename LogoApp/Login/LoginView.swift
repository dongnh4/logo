//
//  LoginView.swift
//  LogoApp
//
//  Created by DongNH on 17/06/2022.
//

import SwiftUI

private enum Constant {
	static let separateLineWidth = 2.0
	static let separateLineHeight = 35.0
	static let spacing = 20.0
}

struct LoginView: View {
	// MARK: - Variables
	@State private var email = ""
	@State private var password = ""

	// MARK: - Body
    var body: some View {
		VStack(spacing: Constant.spacing) {
			ItemLogin("Email", "icon-email", "abc@rikkeisoft.com", textField: email)
			ItemLogin("Mật khẩu", "icon-lock", "Độ dài 6-16 kí tự", textField: password)
			Button {

			} label: {
				Spacer()
				Text("Quên mật khẩu")
					.foregroundColor(Color.purple)
					.padding(.trailing, Constant.separateLineHeight)
			}
		}
		.padding(.top, Constant.spacing)
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
