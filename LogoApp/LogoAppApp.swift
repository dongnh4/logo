//
//  LogoAppApp.swift
//  LogoApp
//
//  Created by đông on 16/06/2022.
//

import SwiftUI

@main
struct LogoAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
