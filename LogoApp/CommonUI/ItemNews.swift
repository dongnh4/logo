//
//  ItemNews.swift
//  LogoApp
//
//  Created by DongNH on 21/06/2022.
//

import SwiftUI

struct ItemNews: View {
    // MARK: - Variables
    private var image1: Image
    private var image2: Image
    private var date: String
    private var titleNews: String
    private var author: String
    private var address: String

    // MARK: - Init
	public init(image1: Image, image2: Image, date: String, titleNews: String, author: String, address: String) {
        self.image1 = image1
        self.image2 = image2
        self.date = date
        self.titleNews = titleNews
        self.author = author
        self.address = address
    }
    
    // MARK: - Body
    var body: some View {
        VStack(spacing: 10) {
			image1.resizable()
				.aspectRatio(contentMode: .fit)
            HStack {
                image2
                Text(date).foregroundColor(Color.gray)
                Spacer()
            }
            .padding(.leading, 15)
            
            Text(titleNews)
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.leading, 15)
                .font(.system(size: 18, weight: .bold))
            
            HStack {
                Text("Bởi \(author)")
                Text("Từ Instagram")
                Spacer()
            }
            .padding(.leading, 15)
            .foregroundColor(Color.gray)
        }
    }
}
