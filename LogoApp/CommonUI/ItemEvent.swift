//
//  ItemEvent.swift
//  LogoApp
//
//  Created by DongNH on 21/06/2022.
//

import SwiftUI

struct ItemEvent: View {
    // MARK: - Variables
//    private var id: 
    private var image1: Image
    private var image2: Image
    private var date: String
    private var titleEvent: String
    private var quantity: String
    private var description: String
    
    // MARK: - Init
    public init(image1: Image, image2: Image, date: String, titleEvent: String, quantity: String, description: String) {
        self.image1 = image1
        self.image2 = image2
        self.date = date
        self.titleEvent = titleEvent
        self.quantity = quantity
        self.description = description
    }
    
    // MARK: - Body
    var body: some View {
        VStack(spacing: 10) {
            image1.resizable()
                .aspectRatio(contentMode: .fit)
            
            HStack {
                image2
                Text("Thứ 4, 2019/12/11")
                Text("•")
                Text("\(quantity) sẽ tham gia")
            }
            Text(titleEvent)
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.leading, 15)
                .font(.system(size: 18, weight: .bold))
            Text(description)
                .padding(.leading, 15)
                .foregroundColor(Color.gray)
        }
    }
}
