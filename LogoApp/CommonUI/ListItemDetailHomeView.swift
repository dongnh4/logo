//
//  ListItemDetailHomeView.swift
//  LogoApp
//
//  Created by VinhLN on 07/07/2022.
//

import SwiftUI

enum Check {
    case time
    case place
    case quantity
    case topic
    case contact
}

struct ListItemDetailHomeView: View {
    // MARK: - Variables
    private var image: String
    private var title: String
    private var type: Check
    private var detail: String
    @State var quantity = ""
    @State var isShowQuantity: Bool = false
    
    // MARK: - Init
    public init(image: String, title: String, detail: String, type: Check) {
        self.image = image
        self.title = title
        self.detail = detail
        self.type = type
    }
    
    var body: some View {
        //        ZStack {
        GeometryReader { geo in
            HStack {
                Image(image)
                    .frame(width: geo.size.width * 0.05, alignment: .leading)
                Text(title)
                    .bold()
                    .frame(width: geo.size.width * 0.3, alignment: .leading)
                Text(detail)
                    .layoutPriority(1)
                    .frame(alignment: .leading)
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            .font(.system(size: 18))
        }
    }
}

struct ListItemDetailHomeView_Previews: PreviewProvider {
    static var previews: some View {
        ListItemDetailHomeView(image: "Group 15-1", title: "ádasd", detail: "dsfdsfs", type: .time)
    }
}
