//
//  OptionButton.swift
//  LogoApp
//
//  Created by DongNH on 17/06/2022.
//

import SwiftUI

struct OptionButton: View {
    // MARK: - Variables
    @State private var firstSelected: Bool
    @State private var secondSelected: Bool
    private var text: String
    private var heightButton: CGFloat

    // MARK: - Init
    public init(_ text: String, _ heightButton: CGFloat, firstSelected: Bool, secondSelected: Bool) {
        self.text = text
        self.heightButton = heightButton
        self.secondSelected = secondSelected
        self.firstSelected = firstSelected
    }
    
    var body: some View {
        VStack {
            Button {
                firstSelected = true
                secondSelected = false
            } label: {
                Text(text)
                    .foregroundColor(firstSelected ? Color.black : Color.gray)
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
            }
            if firstSelected {
                Rectangle()
                    .frame(height: heightButton)
                    .padding(.horizontal, 10)
                    .foregroundColor(Color.purple)
            }
        }
    }
}

//struct OptionButton_Previews: PreviewProvider {
//    static var previews: some View {
//        OptionButton(<#String#>, <#Int#>, action: <#() -> Void#>)
//    }
//}
