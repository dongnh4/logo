//
//  ItemLogin.swift
//  LogoApp
//
//  Created by DongNH on 17/06/2022.
//

import SwiftUI

private enum Constant {
	static let separateLineWidth = 2.0
	static let separateLineHeight = 35.0
	static let sizeImage = 5.0
}

struct ItemLogin: View {
    // MARK: - Variables
	private var image: String
	private var title: String
	private var placeholder: String
	@State private var textField: String

    // MARK: - Init
	public init(_ title: String, _ image: String, _ placeholder: String, textField: String) {
		self.title = title
		self.image = image
		self.placeholder = placeholder
		self.textField = textField
	}

    // MARK: - Body
    var body: some View {
		VStack {
			HStack {
				Image(image)
					.frame(width: Constant.sizeImage, height: Constant.sizeImage)
				Rectangle()
					.frame(width: Constant.separateLineWidth, height: Constant.separateLineHeight)
					.padding(.horizontal, 10)
					.foregroundColor(Color.gray)
				VStack(alignment: .leading) {
					Text(title)
					TextField(placeholder, text: $textField)
				}
			}
		}
		.padding(.horizontal, 20)
    }
}
