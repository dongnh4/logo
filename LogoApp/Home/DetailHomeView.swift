//
//  DetailHomeView.swift
//  LogoApp
//
//  Created by VinhLN on 6/29/22.
//

import SwiftUI

struct DetailHomeView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var event: Event?
    @State var isChange: Bool = false
    let image: String
    let title: String
    let time: String
    @State var quantity: String = ""
    
    var body: some View {
        ZStack {
            ScrollView {
                header
                content
            }
            .navigationTitle("")
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
            .edgesIgnoringSafeArea(.all)
            
            if isChange {
                QuantityView(selectedNum: $quantity, isShowing: $isChange)
                    .frame(maxHeight: .infinity)
            }
        }
    }
}

struct DetailHomeView_Previews: PreviewProvider {
    static var previews: some View {
        DetailHomeView(image: "", title: "", time: "", quantity: "")
            .previewDevice("iPhone 8")
        DetailHomeView(image: "", title: "", time: "", quantity: "")
            .previewDevice("iPhone 11 Pro")
    }
}

extension DetailHomeView {
    var header: some View {
        ZStack(alignment: .topLeading) {
            Image(image)
                .resizable()
                .aspectRatio(contentMode: .fit)
            //                .frame(height: 200)
            Button {
                customBack()
            } label: {
                Image(systemName: "chevron.left")
                    .foregroundColor(Color.white)
                    .padding(.init(top: 35, leading: 20, bottom: 0, trailing: 0))
            }
            
        }
    }
    
    var content: some View {
        VStack(alignment: .leading) {
            Text(title)
                .bold()
                .frame(maxWidth: .infinity, alignment: .leading)
                .font(.system(size: 20))
            
            VStack(spacing: 25) {
                
                ListItemDetailHomeView(image: "Group 15-1", title: "Thời gian", detail: time, type: .time)
                ListItemDetailHomeView(image: "icon-located", title: "Địa điểm", detail: "Rikkeisoft Hà Nội", type: .place)
                ListItemDetailHomeView(image: "users 1", title: "Số lượng", detail: quantity, type: .quantity)
                ListItemDetailHomeView(image: "icon-browse", title: "Chủ đề", detail: "Công nghệ, IT", type: .topic)
                ListItemDetailHomeView(image: "agenda 1", title: "Liên hệ", detail: "Nguyễn Huy Đông", type: .contact)
                
                Button {
                    isChange.toggle()
                } label: {
                    Text("Change quantity")
                        .foregroundColor(Color.white)
                }
                .frame(width: 200, height: 50)
                .background(Color.gray)
                
            }
            Spacer()
        }
        .padding(.horizontal, 10)
    }
}

extension DetailHomeView {
    func customBack() {
        presentationMode.wrappedValue.dismiss()
    }
}
