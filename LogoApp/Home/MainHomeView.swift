//
//  MainHomeView.swift
//  LogoApp
//
//  Created by đông on 19/06/2022.
//

import SwiftUI

private enum Constant {
	static let heightImage = 50.0
	static let heightButtonLogin = 50.0
	static let separateLineHeight = 2.0
}

struct MainHomeView: View {
	// MARK: - Variables
    @State private var didTapNews: Bool = false
	@State private var didTapEvent: Bool = false

	// MARK: - Body
    var body: some View {
			VStack {
				logoImage
				optionButton
				optionView
			}
            .navigationTitle("")
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
            .edgesIgnoringSafeArea(.all)
    }
}

struct MainHomeView_Previews: PreviewProvider {
    static var previews: some View {
        MainHomeView()
    }
}

// MARK: - Extensions
private extension MainHomeView {
	var logoImage: some View {
		ZStack {
			Image("Mask Group (2)")
                .resizable()
                .aspectRatio(contentMode: .fit)
			HStack {
				Text("Trang chủ")
					.foregroundColor(Color.white)
					.padding(.leading, 20)
					.font(.system(size: 20, weight: .bold))
				Spacer()
			}
		}
	}

	var optionButton: some View {
		HStack {
			VStack {
				Button (action: {
					self.didTapNews = true
					self.didTapEvent = false
				}) {
					Text("Tin tức")
						.foregroundColor(didTapNews ? Color.black : Color.gray)
						.frame(maxWidth: .infinity, maxHeight: .infinity)
				}

				if didTapNews {
					Rectangle()
						.frame(height: Constant.separateLineHeight)
						.padding(.horizontal, 10)
						.foregroundColor(Color.purple)
				}
			}

			VStack {
				Button {
					didTapEvent = true
					didTapNews = false
				} label: {
					Text("Sự kiện")
						.foregroundColor(didTapEvent ? Color.black : Color.gray)
						.frame(maxWidth: .infinity, maxHeight: .infinity)
				}
				if didTapEvent {
					Rectangle()
						.frame(height: Constant.separateLineHeight)
						.padding(.horizontal, 10)
						.foregroundColor(Color.purple)
				}
			}
		}
		.frame(maxWidth: .infinity)
		.frame(height: Constant.heightButtonLogin)
		.background(Color.clear)
		.onAppear {
			didTapNews = true
		}
	}
    
    var optionView: some View {
        Group {
            if didTapEvent {
                EventView()
            }
            else {
                NewsView()
            }
        }
    }
}
