//
//  TabView.swift
//  LogoApp
//
//  Created by VinhLN on 6/28/22.
//

import SwiftUI

struct TabBarView: View {
    //MARK: - Body
    var body: some View {
//        NavigationView {
            TabView {
                MainHomeView()
                    .tabItem {
                        Image(systemName: "house.fill")
                        Text("Home")
                    }
                
                CategoryView()
                    .tabItem {
                        Image(systemName: "magnifyingglass")
                        Text("Category")
                    }
            }
//        }
    }
}

struct TabBarView_Previews: PreviewProvider {
    static var previews: some View {
        TabBarView()
            .previewDevice("iPhone 11 Pro")
    }
}

