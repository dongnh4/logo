//
//  NewsView.swift
//  LogoApp
//
//  Created by đông on 19/06/2022.
//

import SwiftUI
import Combine

struct NewsView: View {
    // MARK: - Variables
	@State var news = [News]()
    
    // MARK: - Body
    var body: some View {
        List(news) { item in
			ItemNews(image1: Image(item.image), image2: Image("clock-circular-outline 1"), date: item.date, titleNews: item.title, author: item.author, address: item.address)

        }
		.onAppear() {
			apiCall().getData { (news, _, _) in
				self.news = news
			}
		}
        .listStyle(PlainListStyle())
    }
}

struct NewsView_Previews: PreviewProvider {
	static var previews: some View {
		NewsView()
	}
}
