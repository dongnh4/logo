//
//  Quantity.swift
//  LogoApp
//
//  Created by VinhLN on 11/07/2022.
//

import Foundation

class Quantity: ObservableObject {
    @Published var number: String {
        didSet {
            UserDefaults.standard.set(number, forKey: "number")
        }
    }
    public var numbers = ["0", "20", "25", "30", "35", "40", "45"]
    
    init() {
        self.number = UserDefaults.standard.object(forKey: "number") as? String ?? "0"
    }
}
