//
//  EventView.swift
//  LogoApp
//
//  Created by đông on 19/06/2022.
//

import SwiftUI

struct EventView: View {
    // MARK: - Variables
	@State var events = [Event]()
//    @AppStorage("quantity") private var quantity = ""
    @ObservedObject var quantitySetting = Quantity()
    
    // MARK: - Body
    var body: some View {
        List(events) { event in
            NavigationLink(destination: DetailHomeView(image: event.image, title: event.title, time: event.date, quantity: quantitySetting.number)) {
                ItemEvent(image1: Image(event.image), image2: Image("Group 15"), date: event.date, titleEvent: event.title, quantity: quantitySetting.number, description: event.description)
			}
        }
        .onAppear() {
            apiCall().getData { (_, event, _) in
                self.events = event
            }
        }
		.listStyle(PlainListStyle())
    }
}

struct EventView_Previews: PreviewProvider {
    static var previews: some View {
        EventView()
            .previewDevice("iPhone 8")
        EventView()
            .previewDevice("iPhone 11 Pro")
    }
}
