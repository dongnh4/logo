//
//  QuantityView.swift
//  LogoApp
//
//  Created by VinhLN on 08/07/2022.
//

import SwiftUI

struct QuantityView: View {
    @Binding var selectedNum: String
    @Binding var isShowing: Bool
    @ObservedObject var quantitySetting = Quantity()
    
    init(selectedNum: Binding<String>, isShowing: Binding<Bool>) {
        self._selectedNum = selectedNum
        self._isShowing = isShowing
    }
    var body: some View {
        VStack {
            HStack {
                Button {
                    isShowing = false
                } label: {
                    Image(systemName: "chevron.left")
                        .foregroundColor(.gray)
                }
                Spacer()
            }
            .frame(maxWidth: .infinity)
            
            Text("Select quantity")
                .bold()
                .frame(maxWidth: .infinity, alignment: .leading)
                .font(.system(size: 30))
                .padding(.top, 20)
            
            List(quantitySetting.numbers, id: \.self) { event in
                Button {
                    self.selectedNum = "\(event)"
                    self.quantitySetting.number = "\(event)"
                    isShowing = false
                } label: {
                        Text("\(event)")
                }

            }
            .frame(maxWidth: .infinity, alignment: .leading)
            .listRowBackground(Color.white)
//            .onAppear() {
//                apiCall().getData { (_, event, _) in
//                    self.events = event
//                }
//            }
            .listStyle(PlainListStyle())
            Spacer()
        }
        .background(Color.white)
        .padding(.horizontal, 10)
    }
}

struct QuantityView_Previews: PreviewProvider {
    static var previews: some View {
        QuantityView(selectedNum: .init(projectedValue: .constant("")), isShowing: .init(projectedValue: .constant(false)))
    }
}
