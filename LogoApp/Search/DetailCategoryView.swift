//
//  DetailCategoryView.swift
//  LogoApp
//
//  Created by VinhLN on 6/29/22.
//

import SwiftUI

private enum Constant {
    static let heightImage = 50.0
    static let heightButtonLogin = 50.0
    static let separateLineHeight = 2.0
}

struct DetailCategoryView: View {
    //MARK: - Variables
    @State private var didTapPopular: Bool = false
    @State private var didTapTime: Bool = false
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    let title: String
    let count: Int
    
    var body: some View {
        VStack {
            logoImage
            optionButton
            optionView
            Spacer()
        }
        .navigationTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
        .edgesIgnoringSafeArea(.all)
    }
}

struct DetailCategoryView_Previews: PreviewProvider {
    static var previews: some View {
        DetailCategoryView(title: "Danh mục", count: 0)
    }
}

extension DetailCategoryView {
    var logoImage: some View {
        ZStack {
            Image("Mask Group (2)")
                .resizable()
                .aspectRatio(contentMode: .fit)
            HStack {
                Button {
                    customBack()
                } label: {
                    Image(systemName: "chevron.left")
                        .foregroundColor(Color.white)
                }
                Text("\(title) (\(count))")
                    .foregroundColor(.white)
                Spacer()
                Button {
                    
                } label: {
                    Image(systemName: "magnifyingglass")
                }
                .padding(.trailing)
                .foregroundColor(Color.white)
                
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(.leading)
        }
    }
    
    var optionButton: some View {
        HStack {
            VStack {
                Button (action: {
                    self.didTapPopular = true
                    self.didTapTime = false
                }) {
                    Text("Phổ biến")
                        .foregroundColor(didTapPopular ? Color.black : Color.gray)
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                }

                if didTapPopular {
                    Rectangle()
                        .frame(height: Constant.separateLineHeight)
                        .padding(.horizontal, 10)
                        .foregroundColor(Color.purple)
                }
            }

            VStack {
                Button {
                    didTapTime = true
                    didTapPopular = false
                } label: {
                    Text("Theo thời gian")
                        .foregroundColor(didTapTime ? Color.black : Color.gray)
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                }
                if didTapTime {
                    Rectangle()
                        .frame(height: Constant.separateLineHeight)
                        .padding(.horizontal, 10)
                        .foregroundColor(Color.purple)
                }
            }
        }
        .frame(maxWidth: .infinity)
        .frame(height: Constant.heightButtonLogin)
        .background(Color.clear)
        .onAppear {
            didTapPopular = true
        }
    }
    
    var optionView: some View {
        Group {
            if didTapTime {
                EventView()
            }
            else {
                NewsView()
            }
        }
    }
}

extension DetailCategoryView {
    func customBack() {
        presentationMode.wrappedValue.dismiss()
    }
}

