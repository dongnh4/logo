//
//  CategoryView.swift
//  LogoApp
//
//  Created by đông on 21/06/2022.
//

import SwiftUI

private enum Constant {
	static let heightImage = 50.0
	static let heightButton = 50.0
	static let separateLineHeight = 2.0
	static let spacerLeading = 20.0
}

struct CategoryView: View {
	// MARK: - Variables
	@State var categories = [Category]()
    @State private var isTapButtonSearch: Bool = false
	@Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State private var text: String = ""

	// MARK: - Body
    var body: some View {
		VStack {
			logoImage
			listCategory
            Spacer()
		}
		.edgesIgnoringSafeArea(.all	)
    }
}

struct CategoryView_Previews: PreviewProvider {
    static var previews: some View {
        CategoryView()
    }
}

// MARK: - Extensions
private extension CategoryView {
	var logoImage: some View {
		ZStack {
			Image("Mask Group (2)")
                .resizable()
                .aspectRatio(contentMode: .fit)
            ZStack {
                HStack {
                    Text("Danh mục")
                        .foregroundColor(Color.white)
						.padding(.leading, Constant.spacerLeading)
                        .font(.system(size: Constant.spacerLeading, weight: .bold))
                    Spacer()
                    Button {
                        isTapButtonSearch = true
                    } label: {
                        Image(systemName: "magnifyingglass")
                            .foregroundColor(Color.white)
                            .font(.system(size: Constant.spacerLeading, weight: .bold))
                            .padding(.trailing)
                    }
                }
                if isTapButtonSearch {
                    ZStack {
                        Image("Mask Group (2)")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                        HStack {
                            Button {
								customBack()
                            } label: {
                                Image(systemName: "chevron.left")
									.foregroundColor(Color.white)
                            }

                            TextField("", text: $text)
                                .background(Color.white)
                        }
                        .frame(height: Constant.heightButton)
                        .padding(.horizontal, 15)
                    }
                }
            }
		}
	}

	var listCategory: some View {
		List(categories) { category in
            NavigationLink(destination: DetailCategoryView(title: category.name, count: categories.count)) {
                HStack {
                    Image(category.name)
                        .frame(width: 25, height: Constant.spacerLeading)
                        .padding(.leading, 15)
                    Text(category.name)
                        .padding(.leading, 30)
                        .foregroundColor(Color.black)
                    Spacer()
                }
            }
		}
		.onAppear() {
			apiCall().getData { (_, _, categories) in
				self.categories = categories
			}
		}
		.listStyle(PlainListStyle())
	}
}

// MARK: - Private Func
extension CategoryView {
	func customBack() {
		isTapButtonSearch = false
	}
}
